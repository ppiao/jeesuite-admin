FROM java:8
MAINTAINER jeesuite
ADD @project.build.finalName@.jar @project.build.finalName@.jar
RUN sh -c 'touch /@project.build.finalName@.jar'
ENV JAVA_OPTS=""
CMD exec java $JAVA_OPTS -jar /@project.build.finalName@.jar
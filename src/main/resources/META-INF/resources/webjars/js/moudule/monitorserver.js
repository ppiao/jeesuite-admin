layui.use(['layer', 'laytpl', 'form'],function() {
    var $ = layui.jquery,
    laytpl = layui.laytpl,
    form = layui.form();
    //
    $('.J_search').on('click',function(){
        var loading = layer.load();
        var env = $('#env_select').val();
        $.ajax({
            dataType: "json",
            type: "GET",
            url: '../admin/monitorserver/list/'+env,
            contentType: "application/json",
            complete: function() {
                layer.close(loading);
            },
            success: function(data) {
                if (data.code == 200) {
                    var tpl = $('#list_tpl').html();
                    laytpl(tpl).render(data,
                    function(html) {
                        $("#content").html(html);
                    });
                } else {
                    layer.msg(data.msg, {
                        icon: 5
                    });
                }
            },
            error: function(xhr, type) {
                layer.msg('系统错误', {
                    icon: 5
                });
            }
        });
        return false;
    });
    
    $("#content").on('click', ".J_commond_btn",function() {
    	var cmd = $(this).attr('data-cmd'),dataref = $(this).attr('data-ref');
    	var  params = {};
    	params.env = $('#env_select').val();
    	params.group = $('#group_select').val();
    	params.job = $(this).attr('data-id');
    	
    	var data = dataref ? $(dataref).val() : $(this).attr('data');
    	if(data)params.data = data;
    	
    	layer.confirm('确认执行该操作么？', {
		    btn: ['确定','取消'], //按钮
		    shade: false //不显示遮罩
		}, function(index){
			$.ajax({
	            dataType: "json",
	            type: "POST",
	            url: '../admin/schedule/job/'+cmd,
	            contentType: "application/json",
	            data: JSON.stringify(params),
	            complete: function() {
	                layer.close(index); 
	            },
	            success: function(data) {
	                if (data.code == 200) {
	                	layer.msg(data.msg  || '操作成功', {icon: 5});
	                	//setTimeout(function(){window.location.reload();},500);
	                	$('.J_search').click();
	                }else{
	                	layer.msg(data.msg || '操作失败' , {icon: 5});
	                }
	            },
	            error: function(xhr, type) {
	                layer.msg('系统错误', {
	                    icon: 5
	                });
	            }
	        });
		}, function(index){
	    	layer.close(index); 
		});
        
    });

});
layui.use(['layer', 'laytpl', 'form'],function() {
    var $ = layui.jquery,
    laytpl = layui.laytpl,
    form = layui.form();
    //
    form.on('submit(search)',function(data) {
        var loading = layer.load();
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '../admin/kafka/topicinfos',
            contentType: "application/json",
            data: JSON.stringify(data.field),
            complete: function() {
                layer.close(loading);
            },
            success: function(data) {
                if (data.code == 200) {
                	data = data.data;
                	var tpl;
                	
                	if(data.producer){
                		$('#producer_set').show();
                		var html = '';
                		for(var index in data.producer){
                			html = html + '<tr>';
                			html = html + '<td>'+data.producer[index].group+'</td> ';	
            				html = html + '<td>'+data.producer[index].topic+'</td> ';	
            				html = html + '<td>'+data.producer[index].successNums+'</td> ';	
            				html = html + '<td>'+data.producer[index].errorNums+'</td> ';	
            				html = html + '<td>'+data.producer[index].latestSuccessNums+'</td> ';	
            				html = html + '<td>'+data.producer[index].latestErrorNums+'</td> ';
            				html = html + '<td>'+data.producer[index].source+'</td> ';
            				html = html + '<td>'+data.producer[index].formatLastTime+'</td> ';
            				html = html + '</tr>';
                		}
                		$("#pd_topic_list").html(html);
                	}else{
                		$('#producer_set').hide();
                	}
                	
                	if(data.consumer){
                		$('#consumer_set').show();
                		var html = '';
                		for(var index in data.consumer){
                			var isFirstCol = true;
                			var partitions = data.consumer[index].partitions;
                			for(var index2 in partitions){
                				html = html + '<tr>';
                				if(isFirstCol){
                					html = html + '<td rowspan="'+partitions.length+'" colspan="1">'+partitions[index2].topic+'</td> ';	
                					isFirstCol = false;
                				}
                				html = html + '<td>'+partitions[index2].partition+'</td> ';	
                				html = html + '<td>'+partitions[index2].logSize+'</td> ';	
                				html = html + '<td>'+partitions[index2].offset+'</td> ';	
                				html = html + '<td>'+partitions[index2].lat+'</td> ';	
                				html = html + '<td>'+partitions[index2].owner+'</td> ';	
                				html = html + '<td>'+partitions[index2].createTime+'</td> ';
                				html = html + '<td>'+partitions[index2].formatLastTime+'</td> ';
                				html = html + '</tr>';
                			}
                		}
                		$("#cs_topic_list").html(html);
                	}else{
                		$('#consumer_set').hide();
                	}
                } else {
                    layer.msg(data.msg, {
                        icon: 5
                    });
                }
            },
            error: function(xhr, type) {
                layer.msg('系统错误', {
                    icon: 5
                });
            }
        });
        return false;
    });
    //
    form.on('select(env_select)',function(data) {
        var dataurl = '../admin/kafka/brokers/' + data.value;
        //
        $.getJSON(dataurl,function(result) {
        	var tpl = $('#broker_list_tpl').html();
            laytpl(tpl).render(result,
            function(html) {
                $("#broker_content").html(html);
            });
        });
        //
        dataurl = '../admin/kafka/group/' + data.value;
        $.getJSON(dataurl,
        function(result) {
            var opthtml;
            for (var index in result) {
                if(result[index].text)opthtml = '<option value="' + result[index].value + '">' + result[index].text + '</option>';
                $('#group_select').append(opthtml);
            }
            form.render('select');
        });
    });
    

});
package com.jeesuite.admin.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesuite.admin.model.Menu;
import com.jeesuite.admin.model.SubMenu;
import com.jeesuite.admin.util.SecurityUtil;

@Controller
@RequestMapping("/admin")
public class AdminIndexController {
	

	@Value("${monitor.moudules}")
	private String monitorMoudules;
	
	@RequestMapping(value = "menus", method = RequestMethod.GET)
	public @ResponseBody List<Menu> getMenus(){
		List<Menu> baseMenus = new ArrayList<>();
		Menu menu = null;
		if(SecurityUtil.isSuperAdmin()){			
			menu = new Menu("全局管理", "fa-cubes", true);
			menu.getChildren().add(new SubMenu("profile管理", "&#xe61d;", "profiles/list.html"));
			menu.getChildren().add(new SubMenu("用户管理", "&#xe612;", "user/list.html"));
			baseMenus.add(menu);
		}
		
		menu = new Menu("配置中心", "fa-cubes", !SecurityUtil.isSuperAdmin());
		if(SecurityUtil.isSuperAdmin()){			
			menu.getChildren().add(new SubMenu("应用管理", "&#xe63b;", "configcenter/apps.html"));
		}
		menu.getChildren().add(new SubMenu("配置管理", "&#xe609;", "configcenter/configs.html"));
		menu.getChildren().add(new SubMenu("配置加密工具", "&#xe64e;", "configcenter/encrypt.html"));
		//menu.getChildren().add(new SubMenu("查看生效配置", "&#xe64e;", "configcenter/effective_config.html"));
		baseMenus.add(menu);
		
		if(StringUtils.isNotBlank(monitorMoudules)){
			menu = new Menu("服务监控", "fa-cubes", false);
			menu.getChildren().add(new SubMenu("监控服务配置", "&#xe631;", "monitor/servers.html"));
			if(monitorMoudules.contains("kafka")){
				menu.getChildren().add(new SubMenu("kafka监控", "&#xe633;", "monitor/kafka.html"));
			}
			
			if(monitorMoudules.contains("scheduler")){
				menu.getChildren().add(new SubMenu("定时任务监控", "&#xe641;", "monitor/schedule.html"));
			}
			baseMenus.add(menu);
		}
		
		return baseMenus;
	}


}

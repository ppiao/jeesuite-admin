package com.jeesuite.admin.controller.admin;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesuite.admin.dao.entity.MonitorServerEntity;
import com.jeesuite.admin.dao.entity.MonitorServerEntity.MonitorServerType;
import com.jeesuite.admin.dao.mapper.MonitorServerEntityMapper;
import com.jeesuite.admin.exception.JeesuiteBaseException;
import com.jeesuite.admin.model.SelectOption;
import com.jeesuite.admin.model.WrapperResponseEntity;
import com.jeesuite.admin.util.SecurityUtil;
import com.jeesuite.common.util.ResourceUtils;

@Controller
@RequestMapping("/admin/monitorserver")
public class MonitorServerAdminController {

	private @Autowired MonitorServerEntityMapper monitorServerMapper;
	
	@RequestMapping(value = "types", method = RequestMethod.GET)
	public @ResponseBody List<SelectOption> serverTypes(){
		List<SelectOption> result = new ArrayList<>();
		String moudules = ResourceUtils.getProperty("monitor.moudules");
		if(StringUtils.isNotBlank(moudules)){
			if(moudules.contains("kafka")){
				result.add(new SelectOption("kafka", MonitorServerType.kafka.getDesc()));
				result.add(new SelectOption("kafka:zookeeper", MonitorServerType.kafka_zookeeper.getDesc()));
			}
			
			if(moudules.contains("scheduler")){
				result.add(new SelectOption("scheduler", MonitorServerType.scheduler.getDesc()));
			}
		}
		return result;
	}
	
	@RequestMapping(value = "list/{env}", method = RequestMethod.GET)
	public ResponseEntity<WrapperResponseEntity> listAllByEnv(@PathVariable("env") String env){
		SecurityUtil.requireProfileGanted(env);
		List<MonitorServerEntity> servers = monitorServerMapper.findByEnv(env);
		return new ResponseEntity<WrapperResponseEntity>(new WrapperResponseEntity(servers),HttpStatus.OK); 
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<WrapperResponseEntity> getbyId(@PathVariable("id") int id){
		MonitorServerEntity entity = monitorServerMapper.selectByPrimaryKey(id);
		return new ResponseEntity<WrapperResponseEntity>(new WrapperResponseEntity(entity),HttpStatus.OK); 
	}
	
	@RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
	public ResponseEntity<WrapperResponseEntity> deleteProfile(@PathVariable("id") int id){
		MonitorServerEntity entity = monitorServerMapper.selectByPrimaryKey(id);
		SecurityUtil.requireProfileGanted(entity.getEnv());
		int delete = monitorServerMapper.deleteByPrimaryKey(id);
		return new ResponseEntity<WrapperResponseEntity>(new WrapperResponseEntity(delete > 0),HttpStatus.OK);
	}
	
	@RequestMapping(value = "add", method = RequestMethod.POST)
	public ResponseEntity<WrapperResponseEntity> add(@RequestBody MonitorServerEntity param){
		SecurityUtil.requireProfileGanted(param.getEnv());
	    if(monitorServerMapper.findByEnvAndMoudule(param.getEnv(), param.getMoudule()) != null){
	    	throw new JeesuiteBaseException(1001, "该配置已存在");
	    }
		param.setEnabled(true);
		monitorServerMapper.insertSelective(param);
		return new ResponseEntity<WrapperResponseEntity>(new WrapperResponseEntity(param),HttpStatus.OK);
	}
	
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public ResponseEntity<WrapperResponseEntity> update(@RequestBody MonitorServerEntity param){
		MonitorServerEntity entity = monitorServerMapper.selectByPrimaryKey(param.getId());
		SecurityUtil.requireProfileGanted(entity.getEnv());
		entity.setServers(param.getServers());
		monitorServerMapper.updateByPrimaryKeySelective(entity);
		return new ResponseEntity<WrapperResponseEntity>(new WrapperResponseEntity(true),HttpStatus.OK);
	}
	
}
